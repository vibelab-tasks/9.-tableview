package com.example.hw_9_tableview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import kotlin.random.Random

const val INITIAL_IMAGE_NAME = "bulbasaur"

val imageIDs: Map<String, Int> = mapOf(
    "bulbasaur" to R.drawable.bulbasaur,
    "furret" to R.drawable.furret,
    "haunter" to R.drawable.haunter,
    "mimikyu" to R.drawable.mimikyu,
    "pikachu" to R.drawable.pikachu,
    "umbreon" to R.drawable.umbreon
)

class MainActivity : AppCompatActivity() {
    private lateinit var ivPokemon: ImageView
    private lateinit var btnImageChange: Button
    private lateinit var tvPokemonName: TextView

    private lateinit var imageName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ivPokemon = findViewById(R.id.ivPokemon)
        btnImageChange = findViewById(R.id.btnImageChange)
        tvPokemonName = findViewById(R.id.tvPokemonName)

        imageName = INITIAL_IMAGE_NAME
        changePokemonName()
        changeButtonColor()

        btnImageChange.setOnClickListener {
            changeImage()
            changeButtonColor()
            changePokemonName()
        }
    }

    private fun changePokemonName() {
        tvPokemonName.text = imageName
        val darkImageColor = ColorUtils.blendARGB(getImageColor(), Color.BLACK, 0.2f)
        tvPokemonName.setTextColor( darkImageColor )
    }

    private fun changeButtonColor() {
        btnImageChange.setBackgroundColor( getImageColor() )
    }

    private fun getImageColor(): Int =
        ContextCompat.getColor(this,
            when (imageName) {
                "bulbasaur" -> R.color.bulbasaur
                "furret" -> R.color.furret
                "haunter" -> R.color.haunter
                "mimikyu" -> R.color.mimikyu
                "pikachu" -> R.color.pikachu
                "umbreon" -> R.color.umbreon
                else -> R.color.black
            }
        )


    private fun changeImage() {
        val names = imageIDs.keys.toList()
        val randomImageIndex = Random.nextInt(0, names.size-1)

        var newImageName = names[randomImageIndex]

        if (newImageName == imageName)
            newImageName = names[(randomImageIndex+1)%names.size]

        imageName = newImageName
        val image = imageIDs[newImageName]

        ivPokemon.setImageResource(image!!)
    }

}